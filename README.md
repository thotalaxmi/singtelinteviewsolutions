Section A

Ans:
commit1: (Model files,Solution and JUnit Test Cases for Section A).
src - SolutionA.class, Animal.class, Bird.class, Parrot.class.

test - SolutionATest.class
----------------------------------------------------------------
Section B

commit2: ( Model files,Solution and JUnit Test Cases for Section B).
src - SolutionB.class, Fish.class.

test - SolutionBTest.class

-----------------------------------------------------------------

Section C

commit3: ( Model files,Solution and JUnit Test Cases for Section C).
src - SolutionC.class, Butterfly.class.

test - SolutionCTest.class

------------------------------------------------------------------
Section D

commit4: ( Solution and JUnit Test Cases for Question D).
src - SolutionD.class, Fish.class.

test - SolutionDTest.class

----------------------------------------------------------------
testreport: UnitTestCaseResults/Test Results - com_sigtel_in_SInteviewSolutions.html -  all testcases report ( html )

------------------------------------------------------------------

A picture of a whiteboard or drawing on a paper.

Project Folder[root-path] - diagram.doc