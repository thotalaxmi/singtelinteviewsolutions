package com.sigtel;

//For Question A->4.1, 4.2, 4.3, 4.4
public class Parrot  extends  Bird {

    private  String sound;

    public Parrot(String sound){
        this.sound = sound;
        this.canFly = true;
        this.canSing = true;
        this.canSwim = false;
        this.canWalk = true;
    }

    @Override
    public String sounds(){
        return sound;
    }
}
