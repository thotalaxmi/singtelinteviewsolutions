package com.sigtel;

import java.util.Arrays;

//Solution for Question D
public class SolutionD {
    public static void main(String[] args) {

        //For Question D
        Animal[] animals = new Animal[]{
                new Bird(),
                new Duck(),
                new Chicken(),
                new Rooster(),
                new Parrot("Woof, woof"),
                new Fish(),
                new Shark(),
                new Clownfish(),
                new Dolphin(),
                new  Butterfly (),
                new CaterPillar()
        };

        long flyCount = Arrays.stream(animals).filter(obj -> obj.canFly).count();
        long walkCount = Arrays.stream(animals).filter(obj -> obj.canWalk).count();
        long singCount = Arrays.stream(animals).filter(obj -> obj.canSing).count();
        long swimCount = Arrays.stream(animals).filter(obj -> obj.canSwim).count();

        System.out.println( flyCount);
        System.out.println( walkCount);
        System.out.println( singCount);
        System.out.println( swimCount);
    }
}
