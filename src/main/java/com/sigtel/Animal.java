package com.sigtel;

public abstract class Animal {
    protected boolean canWalk;
    protected  boolean canFly;
    protected  boolean canSing;
    protected  boolean canSwim;

    public boolean isCanWalk() {
        return canWalk;
    }

    public boolean isCanFly() {
        return canFly;
    }

    public boolean isCanSing() {
        return canSing;
    }

    public boolean isCanSwim() {
        return canSwim;
    }

    protected String walk(){
        return (canWalk)?"I am walking":null;
    }

}
