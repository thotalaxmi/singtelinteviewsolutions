package com.sigtel;

//For Question B->1
public class Fish extends  Animal{
    protected String size;
    protected String color;

    public Fish(){
        this.canSing = false;
        this.canWalk = false;
        this.canFly = false;
        this.canSwim = true;
    }

    public String size(){
        return size;
    }

    public String color(){
        return color;
    }

    public String specialCharacteristic(){
        return null;
    }

    public String swim() {
        return (canSwim)?"I am Swimming":null;
    }
}

//For Question B->2
class Shark extends  Fish{
    public Shark(){
        this.size = "Large";
        this.color = "Grey";
    }

    @Override
    public String specialCharacteristic(){
        return "Eats other Fish";
    }
}

class Clownfish  extends  Fish{
    public Clownfish(){
        this.size = "Small";
        this.color = "Orange";
    }

    @Override
    public String specialCharacteristic(){
        return "Makes Jokes";
    }
}

class Dolphin extends  Animal {
    private Fish fish;

    public Dolphin(){
        fish = new Fish();
    }

    public String swim(){
        return fish.swim();
    }
}