package com.sigtel;

import java.awt.*;

//Solution for Question B
public class SolutionB {
    public static void main(String[] args) {
        Fish fish = new Fish();

        /*--------
        Sample Execution for Question B
         */
            //For Question B->1
            System.out.println(fish.canSing); // Return null since fish cannot sing
            System.out.println(fish.canWalk); // Return null since fish cannot walk
            System.out.println(fish.swim()); // Return Swim since fish can Swim

            //For Question B->2
            Fish shark = new Shark();
            Fish clownFish = new Clownfish();
            System.out.println( shark.size() + "--" + shark.color());
            System.out.println( clownFish.size() + "--" + clownFish.color());
            System.out.println( clownFish.specialCharacteristic()); //Makes Jokes
            System.out.println( shark.specialCharacteristic()); //Eats other fishes

            //For Question B->3
            Animal dolphin = new Dolphin();
            System.out.println( ((Dolphin)dolphin).swim());
    }
}
