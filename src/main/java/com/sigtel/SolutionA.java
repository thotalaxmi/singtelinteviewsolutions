package com.sigtel;

//Solution for Question A
public class SolutionA {
    public static void main(String[] args) {
        Bird bird = new Bird();
        bird.walk();
        bird.fly();

        /*--------
        Sample Execution for Question A - 1, 2, 3, 4
         */
            //For Question A->1
            System.out.println(bird.sing());

            //For Question A->2
            Bird duck = new Duck();
            System.out.println( duck.sounds());
            Bird chicken = new Duck();
            System.out.println( chicken.sounds());

            //For Question A->3
            Bird rooster = new Rooster();
            System.out.println( rooster.sounds());

            //For Question A->4
            Parrot parrotWithDogs = new Parrot("Woof, woof");
            Parrot parrotWithCats = new Parrot("Me ow");
            Parrot parrotWithRooster = new Parrot("Cock-a-doodle-doo");

            System.out.println( parrotWithDogs.sounds());
            System.out.println( parrotWithCats.sounds());
            System.out.println( parrotWithRooster.sounds());

            Parrot parrotWithDuck = new Parrot("Quack , quack ");
            System.out.println( parrotWithDuck.sounds());
    }
}
