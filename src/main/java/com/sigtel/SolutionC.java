package com.sigtel;

//Solution for Question C
public class SolutionC {
    public static void main(String[] args) {
        /*--------
        Sample Execution for Question C
         */
            //For Question C->1
            Butterfly butterfly = new Butterfly();
            System.out.println(butterfly.canFly);
            System.out.println(butterfly.canSing);

            //For Question C->1
            Butterfly  caterPillar = new CaterPillar();
            System.out.println(caterPillar.canFly);
            System.out.println(caterPillar.canWalk);
    }
}
