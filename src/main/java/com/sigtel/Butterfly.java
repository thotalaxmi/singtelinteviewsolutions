package com.sigtel;

public class Butterfly extends  Animal{
    public Butterfly(){
        this.canFly = true;
        this.canSing = false;
        this.canSwim = false;
        this.canWalk = false;
    }
}

class CaterPillar extends  Butterfly{
    public CaterPillar(){
        this.canFly= false;
        this.canWalk = true;
    }
}
