package com.sigtel;

//For Question A
public class Bird extends  Animal {
    public Bird(){
        this.canWalk = true;
        this.canFly = true;
        this.canSing = true;
    }

    public String fly() {
        return (canFly)?"I am flying":null;
    }

    //For Question A->1
    public String sing() {
        return (canSing)?"I am Singing":null;
    }
    //For Question A->2
    public String swim() {
        return (canSwim)?"I am Swimming":null;
    }

    //For Question A-2
    public String sounds(){
        return null;
    };
}

//For Question A->2
class Duck extends  Bird{
    public Duck(){
        //For Question A->2.2
        this.canSwim = true;
    }

    //For Question A->2.1
    @Override
    public String sounds(){
        return "Quack, quack";
    }
}

//For Question A->2
class Chicken extends  Bird{
    public Chicken(){
        this.canSwim = false;
        //For Question A->2.4
        this.canFly = false;
    }

    //For Question A->2.3
    @Override
    public String sounds(){
        return "Cluck, cluck";
    }
}

//For Question A->3
class Rooster extends Bird{
    //For Question A->3.2
    private Chicken chicken;

    //For Question A->3.1
    public String sounds(){
        return "Cock-a-doodle-doo";
    }
}