package com.sigtel;

import org.junit.Assert;
import org.junit.Test;

public class SolutionBTest {

    //Test Case for Question B->1
    @Test
    public void testCaseQB1(){
        Fish fish = new Fish();
        Assert.assertEquals(true, fish.canSwim); // Return null since fish cannot sing
        Assert.assertEquals(false,  fish.canWalk ); // Return null since fish cannot walk
        Assert.assertNotNull(fish.swim()); // Return Swim since fish can Swim
    }

    //Test Case for Question B->2
    @Test
    public void testCaseQB2(){
        Fish shark = new Shark();
        Fish clownFish = new Clownfish();

        Assert.assertEquals("Large", shark.size());
        Assert.assertEquals("Grey", shark.color());
        Assert.assertEquals("Small", clownFish.size());
        Assert.assertEquals("Orange", clownFish.color());


        Assert.assertEquals("Makes Jokes", clownFish.specialCharacteristic()); //Makes Jokes
        Assert.assertEquals("Eats other Fish", shark.specialCharacteristic()); //Eats other fishes
    }

    //Test Case for Question B->3
    @Test
    public void testCaseQB3(){
        Animal dolphin = new Dolphin();
        Assert.assertEquals ("I am Swimming", ((Dolphin)dolphin).swim() );
    }
}
