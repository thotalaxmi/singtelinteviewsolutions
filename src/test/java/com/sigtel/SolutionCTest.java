package com.sigtel;

import org.junit.Assert;
import org.junit.Test;

public class SolutionCTest {

    //Test Case for Question C->1
    @Test
    public void testCaseQC1(){
        Butterfly butterfly = new Butterfly();
        Assert.assertEquals(true, butterfly.canFly);
        Assert.assertEquals(false, butterfly.canSing);
    }

    //Test Case for Question C->2
    @Test
    public void testCaseQC2(){
        Butterfly  caterPillar = new CaterPillar();
        Assert.assertEquals(false, caterPillar.canFly);
        Assert.assertEquals(true, caterPillar.canWalk);
    }
}
