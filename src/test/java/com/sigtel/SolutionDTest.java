package com.sigtel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class SolutionDTest {

    Animal[] animals = null;

    @Before
    public void setup(){
        animals = new Animal[]{
                new Bird(),
                new Duck(),
                new Chicken(),
                new Rooster(),
                new Parrot("Woof, woof"),
                new Fish(),
                new Shark(),
                new Clownfish(),
                new Dolphin(),
                new  Butterfly (),
                new CaterPillar()
        };
    }

    //Test Case for Question D->1
    @Test
    public void testCaseQD1(){
        long flyCount = Arrays.stream(animals).filter(obj -> obj.canFly).count();
        Assert.assertEquals(5, flyCount);
    }

    //Test Case for Question D->2
    @Test
    public void testCaseQD2(){
        long walkCount = Arrays.stream(animals).filter(obj -> obj.canWalk).count();
        Assert.assertEquals(6, walkCount);
    }

    //Test Case for Question D->3
    @Test
    public void testCaseQD3(){
        long singCount = Arrays.stream(animals).filter(obj -> obj.canSing).count();
        Assert.assertEquals(5, singCount);
    }

    //Test Case for Question D->4
    @Test
    public void testCaseQD4(){
        long swimCount = Arrays.stream(animals).filter(obj -> obj.canSwim).count();
        Assert.assertEquals(4, swimCount);
    }
}