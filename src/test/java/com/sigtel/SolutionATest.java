package com.sigtel;

import org.junit.Assert;
import org.junit.Test;

public class SolutionATest {

    //Test Case for Question A->1
    @Test
    public void testCaseQA1(){
        Bird bird = new Bird();
        Assert.assertNotNull(bird.sing());
    }

    //Test Case for Question A->2
    @Test
    public void testCaseQA2(){
        Bird duck = new Duck();
        Bird chicken = new Chicken();

        Assert.assertEquals("Quack, quack", duck.sounds());
        Assert.assertEquals("Cluck, cluck", chicken.sounds());
    }

    //Test Case for Question A->3
    @Test
    public void testCaseQA3(){
        Bird rooster = new Rooster();
        System.out.println( rooster.sounds());
        Assert.assertEquals("Cock-a-doodle-doo", rooster.sounds());
    }

    //Test Case for Question A->4
    @Test
    public void testCaseQA4(){
        Parrot parrotWithDogs = new Parrot("Woof, woof");
        Parrot parrotWithCats = new Parrot("Me ow");
        Parrot parrotWithRooster = new Parrot("Cock-a-doodle-doo");

        Assert.assertEquals("Woof, woof", parrotWithDogs.sounds());
        Assert.assertEquals("Me ow", parrotWithCats.sounds());
        Assert.assertEquals("Cock-a-doodle-doo", parrotWithRooster.sounds());

        Parrot parrotWithDuck = new Parrot("Quack , quack");
        Assert.assertEquals("Quack , quack", parrotWithDuck.sounds());
    }
}
